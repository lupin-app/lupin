package dev.lupin.app.team.dto;

import dev.lupin.app.base.LupinDTO;
import dev.lupin.app.domain.team.TeamRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MemberDTO extends LupinDTO {

    private LocalDate startDate;

    private LocalDate endDate;

    private boolean extern;

    private TeamRoleDTO role;
}
