package dev.lupin.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LupinAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(LupinAppApplication.class, args);
	}

}
