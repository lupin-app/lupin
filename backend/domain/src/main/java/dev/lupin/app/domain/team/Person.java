package dev.lupin.app.domain.team;

import dev.lupin.app.domain.base.LupinAuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "te_p")
public class Person  extends LupinAuditEntity {
    @Column(name = "b_a")
    private String lastname;

    @Column(name = "b_b")
    private String firstname;

    @Column(name = "b_c")
    private String email;

    @OneToMany(mappedBy = "person", fetch = FetchType.LAZY)
    private List<TeamMember> teams;
}
