package dev.lupin.app.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Sort {

    public static final Direction DEFAULT_DIRECTION = Direction.ASC;

    private List<Order> orders;

    public Sort(List<Order> orders) {
        this.orders = orders;
    }

    public Sort(Direction direction, List<String> properties) {

        this.orders = properties.stream() //
                .map(it -> new Order(it, direction)) //
                .collect(Collectors.toList());
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Order {

        private String column;

        private Direction direction;

    }

    public enum Direction {ASC, DESC;}
}
