package dev.lupin.app.domain.team;

public enum TeamRole {
    OWNER,
    TEAM_MANAGER,
    MEMBER,
    REPORTER,
    GUEST;
}
