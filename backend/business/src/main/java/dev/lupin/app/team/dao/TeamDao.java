package dev.lupin.app.team.dao;

import dev.lupin.app.base.CrudDao;
import dev.lupin.app.team.dto.TeamDTO;

public interface TeamDao extends CrudDao<TeamDTO>  {
}
