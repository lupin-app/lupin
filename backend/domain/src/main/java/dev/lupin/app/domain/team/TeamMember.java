package dev.lupin.app.domain.team;

import dev.lupin.app.domain.base.LupinAuditEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "te_m")
public class TeamMember extends LupinAuditEntity {

    @Column(name = "b_a")
    private Team team;

    @Column(name = "b_b")
    private Person person;

    @Column(name = "b_c")
    private LocalDate startDate;

    @Column(name = "b_d")
    private LocalDate endDate;

    @Column(name = "b_e")
    private boolean extern;

    @Column(name = "b_f")
    private TeamRole role;

}
