## Details
- **Use case**: `FEATURE_NAME`

--------------------------------------------------------------------------------

## 1. Introduction

`quick use case/feature presentation`

## 2. Simpliest

`Simpliest version that can be implmented`


## 3. Upcoming

`List future evolutions for the feature`

## 4. Potential

`List potential future evolutions for the feature`

/label ~"State::TO DO" ~"Type::Use case"
