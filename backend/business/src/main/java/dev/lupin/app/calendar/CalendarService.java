package dev.lupin.app.calendar;

import dev.lupin.app.calendar.dto.CalendarEntry;

import java.time.LocalDate;
import java.util.List;

public interface CalendarService {

    /**
     * Return a list of entries representing each days for the month with complementary information
     * @param year The year
     * @param month The month
     * @return THe list of entries
     */
    List<CalendarEntry> getCalendarForMonth(int year, int month);

    /**
     * Return the number of days in a month
     * @param year The year
     * @param month The month
     * @return The number of days in a month
     */
    int getNumberDayForMonth(int year, int month);

    /**
     * Compute The easter date
     * https://en.wikipedia.org/wiki/Computus
     * @param year The year
     * @return The sunday easter date
     */
    LocalDate computeEasterDate(int year);
}
