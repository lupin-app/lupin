package dev.lupin.app.base;

import java.util.stream.Collectors;

public class PageSortBuilder {

    private static org.springframework.data.domain.Sort.Direction mapDirection(Sort.Direction direction) {
        switch (direction) {
            case ASC:
                return org.springframework.data.domain.Sort.Direction.ASC;
            case DESC:
                return org.springframework.data.domain.Sort.Direction.DESC;
            default:
                return null;
        }
    }

    private static org.springframework.data.domain.Sort.Order mapOrder(Sort.Order order) {
        return new org.springframework.data.domain.Sort.Order(mapDirection(order.getDirection()), order.getColumn());
    }


    public static org.springframework.data.domain.Sort buildSort(Sort sort) {
        return org.springframework.data.domain.Sort.by(sort.getOrders().stream().map(PageSortBuilder::mapOrder).collect(Collectors.toList()));
    }
}
