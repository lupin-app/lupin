package dev.lupin.app.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DaoRepository<E> extends JpaRepository<E, UUID>, JpaSpecificationExecutor<E> {

    Optional<E> findByTechnicalId(UUID id);

    Optional<E> findByDatabaseId(Long id);

    Optional<E> findByBusinessId(String id);

    void deleteByTechnicalId(UUID id);

    void deleteByDatabaseId(Long id);

    void deleteByBusinessId(String id);

    List<E> findAllByTechnicalId(List<UUID> id);

    List<E> findAllByDatabaseId(List<Long> id);

    List<E> findAllByBusinessId(List<String> id);
}
