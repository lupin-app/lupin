package dev.lupin.app.team.dao.impl;

import dev.lupin.app.base.CrudDaoImpl;
import dev.lupin.app.domain.team.Person;
import dev.lupin.app.team.dao.PersonDao;
import dev.lupin.app.team.dao.mapper.PersonMapper;
import dev.lupin.app.team.dto.PersonDTO;
import dev.lupin.app.team.repository.PersonRepository;
import org.springframework.stereotype.Service;

@Service
class PersonDaoImpl extends CrudDaoImpl<PersonDTO, Person> implements PersonDao {

    protected PersonDaoImpl(PersonRepository repository, PersonMapper mapper) {
        super(repository, mapper);
    }

    @Override
    protected Person updateImpl(PersonDTO dto, Person entity) {
        return entity;
    }
}
