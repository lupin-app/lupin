package dev.lupin.app.base;

import dev.lupin.app.domain.base.LupinAuditEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public abstract class CrudDaoImpl<DTO extends LupinDTO, E extends LupinAuditEntity> implements CrudDao<DTO> {

    protected final DaoRepository<E> repository;
    protected final DaoMapper<DTO, E > mapper;

    protected int defaultPage = 1;
    protected int defaultSize = 20;

    protected CrudDaoImpl(DaoRepository<E> repository, DaoMapper<DTO, E> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    protected DTO buildDto(E e) {
        return mapper.entityToDto(e);
    }

    protected E buildEntity(DTO dto) {
        return mapper.dtoToEntity(dto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DTO> findByTechnicalId(final UUID id) {
        return this.repository.findByTechnicalId(id).map(this::buildDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DTO> findByDatabaseId(final Long id) {
        return this.repository.findByDatabaseId(id).map(this::buildDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DTO> findByBusinessId(final String id) {
        return this.repository.findByBusinessId(id).map(this::buildDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DTO> findAll() {
        return this.repository.findAll().stream().map(this::buildDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<DTO> findAll(final Sort sort) {
        return this.repository.findAll(PageSortBuilder.buildSort(sort)).stream().map(this::buildDto).collect(Collectors.toList());
    }

    protected void setDefaultParam(final PageParam param) {
        if (param.getPage() == null) {
            param.setPage(this.defaultPage);
        }
        if (param.getSize() == null) {
            param.setSize(this.defaultSize);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DTO> findAll(final PageParam param) {
        setDefaultParam(param);

        PageRequest request = PageRequest.of(param.getPage(), param.getSize(), PageSortBuilder.buildSort(param.getSort()));

        org.springframework.data.domain.Page<E> page = this.repository.findAll(request);

        final List<DTO> mappedDto = page.map(this::buildDto).getContent();

        return new Page<>(page.getTotalPages(), param.getPage(), param.getSize(), page.getTotalElements(), mappedDto);
    }

    @Override
    @Transactional
    public DTO save(final DTO dto) {
        return this.buildDto(this.repository.save(this.buildEntity(dto)));
    }

    protected abstract E updateImpl(final DTO dto, final E entity);

    @Override
    @Transactional
    public Optional<DTO> updateByTechnicalId(final UUID id, final DTO dto) {
        Optional<DTO> result = Optional.empty();
        final Optional<E> optionalEntity = repository.findByTechnicalId(id);
        if (optionalEntity.isPresent()) {
            E entity = updateImpl(dto, optionalEntity.get());
            entity = repository.save(entity);
            result = Optional.of(buildDto(entity));
        }
        return result;
    }

    @Override
    @Transactional
    public Optional<DTO> updateByDatabaseId(final Long id, final DTO dto) {
        Optional<DTO> result = Optional.empty();
        final Optional<E> optionalEntity = repository.findByDatabaseId(id);
        if (optionalEntity.isPresent()) {
            E entity = updateImpl(dto, optionalEntity.get());
            entity = repository.save(entity);
            result = Optional.of(buildDto(entity));
        }
        return result;
    }

    @Override
    @Transactional
    public Optional<DTO> updateByBusinessId(final String id, final DTO dto) {
        Optional<DTO> result = Optional.empty();
        final Optional<E> optionalEntity = repository.findByBusinessId(id);
        if (optionalEntity.isPresent()) {
            E entity = updateImpl(dto, optionalEntity.get());
            entity = repository.save(entity);
            result = Optional.of(buildDto(entity));
        }
        return result;
    }

    @Override
    public void deleteByTechnicalId(final UUID id) {
        this.repository.deleteByTechnicalId(id);
    }

    @Override
    public void deleteByDatabaseId(final Long id) {
        this.repository.deleteByDatabaseId(id);
    }

    @Override
    public void deleteByBusinessId(final String id) {
        this.repository.deleteByBusinessId(id);
    }

    @Override
    public void deleteAllByTechnicalId(final List<UUID> id) {
        this.repository.findAllByTechnicalId(id).forEach(this.repository::delete);
    }

    @Override
    public void deleteAllByDatabaseId(final List<Long> id) {
        this.repository.findAllByDatabaseId(id).forEach(this.repository::delete);
    }

    @Override
    public void deleteAllByBusinessId(final List<String> id) {
        this.repository.findAllByBusinessId(id).forEach(this.repository::delete);
    }
}
