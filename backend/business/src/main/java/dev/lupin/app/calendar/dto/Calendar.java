package dev.lupin.app.calendar.dto;

import dev.lupin.app.base.LupinDTO;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.TimeZone;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Calendar extends LupinDTO {

    private String name;

    private TimeZone timeZone;

    private List<Event> events;
}
