# Lupin backend application

## Project Structure 

* **domain** : The domain entity definition for the application
* **dao** : The repository to use the domain and the dao service
* **utility** : The utility code for the app like common classes
* **business** : The business specific classes
* **web** : The API demfinition for the project
* **runner** : The configuration for the project

## Dependencies :

Below the dependencies between projects

* **domain** : none
* **dao** : 
  - domain
  - business
* **utility** : none 
* **business** : 
  - domain
  - utilisty
* **web** : 
  - business
* **runner** : 
  - domain
  - dao
  - utility
  - business
  - web
  