package dev.lupin.app.domain.base;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@MappedSuperclass
@Getter
@Setter
public class LupinEntity {

    @Id
    @Column(name = "a_a", nullable = false, unique = true, updatable = false)
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    @NotNull
    private UUID technicalId;

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @NotNull
    @Column(name = "a_b", nullable = false, unique = true, updatable = false, insertable = false)
    private Long databaseId;

    @NotNull
    @Column(name = "a_c", nullable = false, unique = true, updatable = false)
    private String businessId;

}
