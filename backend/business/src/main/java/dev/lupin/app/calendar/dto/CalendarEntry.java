package dev.lupin.app.calendar.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CalendarEntry {

    private int dayOfMonth;
    private int dayOfWeek;
    private int yearWeek;
    private int monthWeek;
    private int month;
    private int year;

    private WeekDay dayOfWeekName;
    private Month monthName;
}
