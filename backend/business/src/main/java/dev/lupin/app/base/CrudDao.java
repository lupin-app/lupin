package dev.lupin.app.base;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CrudDao<DTO> {

    // Find

    Optional<DTO> findByTechnicalId(final UUID id);

    Optional<DTO> findByDatabaseId(final Long id);

    Optional<DTO> findByBusinessId(final String id);

    List<DTO> findAll();

    List<DTO> findAll(final Sort sort);

    Page<DTO> findAll(final PageParam param);

    // Save

    DTO save(final DTO dto);

    // Update

    Optional<DTO> updateByTechnicalId(final UUID id, final DTO dto);

    Optional<DTO> updateByDatabaseId(final Long id, final DTO dto);

    Optional<DTO> updateByBusinessId(final String id, final DTO dto);

    // Delete

    void deleteByTechnicalId(final UUID id);

    void deleteByDatabaseId(final Long id);

    void deleteByBusinessId(final String id);

    void deleteAllByTechnicalId(final List<UUID> id);

    void deleteAllByDatabaseId(final List<Long> id);

    void deleteAllByBusinessId(final List<String> id);
}
