package dev.lupin.app.calendar.impl;

import dev.lupin.app.calendar.CalendarService;
import dev.lupin.app.calendar.dto.CalendarEntry;
import dev.lupin.app.calendar.dto.Month;
import dev.lupin.app.calendar.dto.WeekDay;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
class CalendarServiceImpl implements CalendarService {

    /**
     * Return a list of entries representing each days for the month with complementatry information
     * @param year The year
     * @param month The month
     * @return THe list of entries
     */
    @Override
    public List<CalendarEntry> getCalendarForMonth(int year, int month) {
        List<CalendarEntry> entries = new ArrayList<>();

        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY, 4);
        for (int i = 1; i <= getNumberDayForMonth(year, month); i++) {
            LocalDate day = LocalDate.of(year, month, i);

            CalendarEntry entry = CalendarEntry.builder()
                    .dayOfMonth(i)
                    .month(month)
                    .year(year)
                    .yearWeek(day.get(weekFields.weekOfWeekBasedYear()))
                    .monthWeek(day.get(weekFields.weekOfMonth()))
                    .monthName(Month.valueOf(day.getMonth().name()))
                    .dayOfWeek(day.getDayOfWeek().ordinal()+1)
                    .dayOfWeekName(WeekDay.valueOf(day.getDayOfWeek().name()))
                    .build();

            entries.add(entry);
        }

        return entries;
    }

    /**
     * Return the number of days in a month
     * @param year The year
     * @param month The month
     * @return The number of days in a month
     */
    @Override
    public int getNumberDayForMonth(int year, int month) {
        switch (month) {
            case 2:
                return (year % 4 == 0) ? 29 : 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 31;
        }
    }

    /**
     * Compute The easter date
     * https://en.wikipedia.org/wiki/Computus
     * @param year The year
     * @return The sunday easter date
     */
    @Override
    public LocalDate computeEasterDate(int year) {
        int n = year % 19;
        int c = year / 100;
        int u = year % 100;
        int s = c / 4;
        int t = c % 4;
        int p = (c + 8) / 25;
        int q = (c-p +1) / 3;
        int e = ((19*n)+c-s-q+15) % 30;
        int b = u / 4;
        int d = u % 4;
        int l = ((2*t)+(2*b)-e-d+32) % 7;
        int h = (n+(11*e)+(22*l)) / 451;
        int mj = (e+l-(7*h)+114);
        int m = mj / 31;
        int j = mj % 31;

        if(m ==3 ||m == 4)
            j++;

        return LocalDate.of(year, m, j);
    }
}
