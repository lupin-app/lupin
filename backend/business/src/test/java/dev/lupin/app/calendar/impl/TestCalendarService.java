package dev.lupin.app.calendar.impl;

import dev.lupin.app.calendar.dto.CalendarEntry;
import dev.lupin.app.calendar.dto.Month;
import dev.lupin.app.calendar.dto.WeekDay;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class TestCalendarService {

    private static CalendarServiceImpl calendarService;

    @BeforeAll
    public static void setUp() {
        calendarService = new CalendarServiceImpl();
    }

    @Test
    public void testNumberOfDaysFebruary() {
        int[] years = {2000, 2020, 2021};
        int[] month = {2, 2, 2};
        int[] expected = {29, 29, 28};

        for (int i = 0; i < years.length; i++) {
            int actual = calendarService.getNumberDayForMonth(years[i], month[i]);
            Assertions.assertEquals(expected[i], actual);
        }
    }

    @Test
    public void testNumberOfDays() {
        int[] expected = {31,28,31,30,31,30,31,31,30,31,30,31};

        for(int i = 1; i <= 12; i++) {
            int actual = calendarService.getNumberDayForMonth(2021, i);
            Assertions.assertEquals(expected[i-1], actual);
        }
    }

    @Test
    public void testFebruary2021() {
        List<CalendarEntry> days = calendarService.getCalendarForMonth(2021, 2);

        Assertions.assertEquals(28, days.size(), "should contain 28 days");
        CalendarEntry firstDay = days.get(0);
        CalendarEntry lastDay = days.get(27);

        CalendarEntry expected = CalendarEntry.builder()
                .dayOfMonth(1)
                .month(2)
                .monthName(Month.FEBRUARY)
                .monthWeek(1)
                .year(2021)
                .dayOfWeek(1)
                .dayOfWeekName(WeekDay.MONDAY)
                .yearWeek(5).build();

        Assertions.assertEquals(expected, firstDay, "should be equals");

        CalendarEntry expected2 = CalendarEntry.builder()
                .dayOfMonth(28)
                .month(2)
                .monthName(Month.FEBRUARY)
                .monthWeek(4)
                .year(2021)
                .dayOfWeek(7)
                .dayOfWeekName(WeekDay.SUNDAY)
                .yearWeek(8).build();

        Assertions.assertEquals(expected2, lastDay, "should be equals");
    }

    @Test
    public void testComputeEasterDate() {
        Map<Integer, LocalDate> entries = Map.of(
                2001, LocalDate.of(2001,4,15),
                2004, LocalDate.of(2004,4,11),
                2006, LocalDate.of(2006,4,16),
                2007, LocalDate.of(2007,4,8),
                2010, LocalDate.of(2010,4,4),
                2011, LocalDate.of(2011,4,24),
                2014, LocalDate.of(2014,4,20),
                2017, LocalDate.of(2017,4,16),
                2018, LocalDate.of(2018,4,1),
                2021, LocalDate.of(2021,4,4)
                );

        for(var entry : entries.entrySet()) {
            var result = calendarService.computeEasterDate(entry.getKey());
            Assertions.assertEquals(entry.getValue(), result, "should be equals");
        }
    }
}
