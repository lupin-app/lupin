package dev.lupin.app.team.impl;

import dev.lupin.app.base.Page;
import dev.lupin.app.base.PageParam;
import dev.lupin.app.team.TeamService;
import dev.lupin.app.team.dao.TeamDao;
import dev.lupin.app.team.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
class TeamServiceImpl implements TeamService {

    private final TeamDao teamDao;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Team management
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Create a new team and add the creator to that team.
     *
     * @param team    The team
     * @param creatorBusinessId The team creator business ID
     * @return The created team
     */
    @Override
    public TeamDTO createTeam(TeamDTO team, String creatorBusinessId) {
        TeamDTO teamDTO = teamDao.save(team);
        MemberDTO role = MemberDTO.builder()
                .startDate(LocalDate.now())
                .extern(false)
                .role(TeamRoleDTO.OWNER)
                .build();

        addMember(teamDTO.getBusinessId(), creatorBusinessId, role);
        return teamDTO;
    }

    /**
     * Update the team.
     *
     * @param team The team
     * @return The updated team
     */
    @Override
    public TeamDTO updateTeam(TeamDTO team) {
        return teamDao.save(team);
    }

    /**
     * Check if a team can be deleted and delete it.
     *
     * @param businessId The team business ID
     * @return TRUE is the team is successfully deleted else FALSE
     */
    @Override
    public boolean deleteTeam(String businessId) {
        return false;
    }

    /**
     * List the available team roles.
     *
     * @param businessId The team business ID
     * @return The available team roles
     */
    @Override
    public List<TeamRoleDTO> getAvailableTeamRole(String businessId) {
        return null;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Team finder
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get the team information.
     *
     * @param businessId The team business ID
     * @return The team information
     */
    @Override
    public TeamDTO getTeam(String businessId) {
        return teamDao.findByBusinessId(businessId).orElseThrow(IllegalAccessError::new);
    }

    /**
     * List all the team
     *
     * @return The complete list of team.
     */
    @Override
    public List<TeamDTO> getAllTeam() {
        return teamDao.findAll();
    }

    /**
     * List all the team.
     *
     * @param pageParam The page parameters
     * @return A page contain part of the team list
     */
    @Override
    public Page<TeamDTO> getAllTeam(PageParam pageParam) {
        return teamDao.findAll(pageParam);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Team member management
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * List all the members, past and present, for the given team.
     *
     * @param businessId The team business ID
     * @return The complete list of members
     */
    @Override
    public List<TeamMemberDTO> getAllMemberForTeam(String businessId) {
        return null;
    }

    /**
     * List all the members, past and present, for the given team.
     *
     * @param businessId The team business ID
     * @param pageParam  The page parameters
     * @return A page contain part of the members list
     */
    @Override
    public Page<TeamMemberDTO> getAllMemberForTeam(String businessId, PageParam pageParam) {
        return null;
    }

    /**
     * List the current members for the given team.
     *
     * @param businessId The team business ID
     * @return The complete list of members
     */
    @Override
    public List<TeamMemberDTO> getAllCurrentMemberForTeam(String businessId) {
        return null;
    }

    /**
     * List the current members for the given team.
     *
     * @param businessId The team business ID
     * @param pageParam  The page parameters
     * @return A page contain part of the members list
     */
    @Override
    public Page<TeamMemberDTO> getAllCurrentMemberForTeam(String businessId, PageParam pageParam) {
        return null;
    }

    /**
     * Add a new member on the team.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The person business ID
     * @param access           The access for that person
     * @return The TeamMember DTO created
     */
    @Override
    public TeamMemberDTO addMember(String teamBusinessId, String personBusinessId, MemberDTO access) {
        return null;
    }

    /**
     * Update a member on the team.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The person business ID
     * @param access           The access for that person
     * @return The TeamMember DTO updated
     */
    @Override
    public TeamMemberDTO updateMember(String teamBusinessId, String personBusinessId, MemberDTO access) {
        return null;
    }

    /**
     * Revoke a member from the team.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The team business ID
     * @return The TeamMember DTO updated
     */
    @Override
    public TeamMemberDTO revokeMember(String teamBusinessId, String personBusinessId) {
        return null;
    }

    /**
     * Promote a member from the team.
     * Stop the current access and grant the new one.
     * After the end of those new access, if the old access are still valid, the member will get those old access back.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The team business ID
     * @param access           The new access for that person
     * @return The TeamMember DTO updated
     */
    @Override
    public TeamMemberDTO promoteMember(String teamBusinessId, String personBusinessId, MemberDTO access) {
        return null;
    }
}
