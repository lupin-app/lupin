package dev.lupin.app.team.dao;

import dev.lupin.app.base.CrudDao;
import dev.lupin.app.team.dto.PersonDTO;

public interface PersonDao extends CrudDao<PersonDTO> {
}
