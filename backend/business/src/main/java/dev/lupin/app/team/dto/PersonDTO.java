package dev.lupin.app.team.dto;

import dev.lupin.app.base.LupinDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class PersonDTO extends LupinDTO {

    private String lastname;

    private String firstname;

    private String email;
}
