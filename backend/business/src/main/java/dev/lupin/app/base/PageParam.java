package dev.lupin.app.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageParam {

    private Integer page;

    private Integer size;

    private String search;

    private Sort sort;
}
