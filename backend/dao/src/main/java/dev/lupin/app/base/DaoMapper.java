package dev.lupin.app.base;

import dev.lupin.app.domain.base.LupinAuditEntity;

public interface DaoMapper<DTO extends LupinDTO, E extends LupinAuditEntity> {

    DTO entityToDto(final E e);

    E dtoToEntity(final DTO dto);
}
