package dev.lupin.app.team.repository;

import dev.lupin.app.domain.team.Team;
import dev.lupin.app.base.DaoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends DaoRepository<Team> {
}
