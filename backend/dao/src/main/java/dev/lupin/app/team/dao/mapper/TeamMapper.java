package dev.lupin.app.team.dao.mapper;

import dev.lupin.app.base.DaoMapper;
import dev.lupin.app.domain.team.Team;
import dev.lupin.app.team.dto.TeamDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy= ReportingPolicy.IGNORE)
public interface TeamMapper extends DaoMapper<TeamDTO, Team> {

    TeamDTO entityToDto(final Team team);

    Team dtoToEntity(final TeamDTO dto);
}
