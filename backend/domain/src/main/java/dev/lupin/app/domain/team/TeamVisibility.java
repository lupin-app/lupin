package dev.lupin.app.domain.team;

public enum TeamVisibility {
    CONFIDENTIAL,
    PRIVATE,
    INTERNAL,
    PUBLIC;
}