package dev.lupin.app.base;

import lombok.*;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Page<DTO> {

    private int totalPages;
    private int index;
    private int size;
    private long totalElements;
    private List<DTO> content;

    private Page(Page<?> page, List<DTO> content) {
        this(page.totalPages, page.index, page.size, page.totalElements, content);
    }

    <U> Page<U> map(Function<? super DTO, ? extends U> converter) {
        return new Page<>(this, getConvertedContent(converter));
    }

    public Stream<DTO> stream() {
        return this.content.stream();
    }

    public Iterator<DTO> iterator() {
        return content.iterator();
    }

    protected <U> List<U> getConvertedContent(Function<? super DTO, ? extends U> converter) {
        return this.stream().map(converter).collect(Collectors.toList());
    }

}
