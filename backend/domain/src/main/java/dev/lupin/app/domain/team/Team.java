package dev.lupin.app.domain.team;

import dev.lupin.app.domain.base.LupinAuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "te_t")
public class Team extends LupinAuditEntity {
    @Column(name = "b_a")
    private String name;

    @Column(name = "b_b")
    private TeamVisibility visibility;

    @Column(name = "b_c")
    private String namespace;

    @Column(name = "b_d")
    private String description;

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY)
    private List<Team> subTeams;

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY)
    private List<TeamMember> members;
}
