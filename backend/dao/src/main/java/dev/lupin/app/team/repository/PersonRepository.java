package dev.lupin.app.team.repository;

import dev.lupin.app.domain.team.Person;
import dev.lupin.app.base.DaoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends DaoRepository<Person> {
}
