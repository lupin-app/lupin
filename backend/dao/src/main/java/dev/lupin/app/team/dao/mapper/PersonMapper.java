package dev.lupin.app.team.dao.mapper;

import dev.lupin.app.base.DaoMapper;
import dev.lupin.app.domain.team.Person;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import dev.lupin.app.team.dto.PersonDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy= ReportingPolicy.IGNORE)
public interface PersonMapper extends DaoMapper<PersonDTO, Person> {

    PersonDTO entityToDto(final Person person);

    Person dtoToEntity(final PersonDTO dto);
}
