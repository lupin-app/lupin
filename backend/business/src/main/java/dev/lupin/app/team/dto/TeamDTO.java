package dev.lupin.app.team.dto;

import dev.lupin.app.domain.team.TeamVisibility;
import dev.lupin.app.base.LupinDTO;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class TeamDTO extends LupinDTO {

    private String name;

    private TeamVisibility visibility;

    private String namespace;

    private String description;
}
