package dev.lupin.app.team.dao.impl;

import dev.lupin.app.base.CrudDaoImpl;
import dev.lupin.app.team.dao.mapper.TeamMapper;
import dev.lupin.app.team.dao.TeamDao;
import dev.lupin.app.domain.team.Team;
import dev.lupin.app.team.dto.TeamDTO;
import dev.lupin.app.team.repository.TeamRepository;
import org.springframework.stereotype.Service;

@Service
class TeamDaoImpl extends CrudDaoImpl<TeamDTO, Team> implements TeamDao {

    protected TeamDaoImpl(TeamRepository repository, TeamMapper mapper) {
        super(repository, mapper);
    }

    @Override
    protected Team updateImpl(TeamDTO dto, Team entity) {
        return entity;
    }
}
