package dev.lupin.app.team;

import dev.lupin.app.base.Page;
import dev.lupin.app.base.PageParam;
import dev.lupin.app.team.dto.*;

import java.util.List;

public interface TeamService {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Team management
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Create a new team and add the creator to that team.
     *
     * @param team    The team
     * @param creatorBusinessId The team creator business ID
     * @return The created team
     */
    TeamDTO createTeam(TeamDTO team, String creatorBusinessId);

    /**
     * Update the team.
     *
     * @param team The team
     * @return The updated team
     */
    TeamDTO updateTeam(TeamDTO team);

    /**
     * Check if a team can be deleted and delete it.
     *
     * @param businessId The team business ID
     * @return TRUE is the team is successfully deleted else FALSE
     */
    boolean deleteTeam(String businessId);

    /**
     * List the available team roles.
     *
     * @param businessId The team business ID
     * @return The available team roles
     */
    List<TeamRoleDTO> getAvailableTeamRole(String businessId);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Team finder
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get the team information.
     *
     * @param businessId The team business ID
     * @return The team information
     */
    TeamDTO getTeam(String businessId);

    /**
     * List all the team
     *
     * @return The complete list of team.
     */
    List<TeamDTO> getAllTeam();

    /**
     * List all the team.
     *
     * @param pageParam The page parameters
     * @return A page contain part of the team list
     */
    Page<TeamDTO> getAllTeam(PageParam pageParam);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Team member management
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * List all the members, past and present, for the given team.
     *
     * @param businessId The team business ID
     * @return The complete list of members
     */
    List<TeamMemberDTO> getAllMemberForTeam(String businessId);

    /**
     * List all the members, past and present, for the given team.
     *
     * @param businessId The team business ID
     * @param pageParam  The page parameters
     * @return A page contain part of the members list
     */
    Page<TeamMemberDTO> getAllMemberForTeam(String businessId, PageParam pageParam);

    /**
     * List the current members for the given team.
     *
     * @param businessId The team business ID
     * @return The complete list of members
     */
    List<TeamMemberDTO> getAllCurrentMemberForTeam(String businessId);

    /**
     * List the current members for the given team.
     *
     * @param businessId The team business ID
     * @param pageParam  The page parameters
     * @return A page contain part of the members list
     */
    Page<TeamMemberDTO> getAllCurrentMemberForTeam(String businessId, PageParam pageParam);

    /**
     * Add a new member on the team.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The person business ID
     * @param access           The access for that person
     * @return The TeamMember DTO created
     */
    TeamMemberDTO addMember(String teamBusinessId, String personBusinessId, MemberDTO access);

    /**
     * Update a member on the team.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The person business ID
     * @param access           The access for that person
     * @return The TeamMember DTO updated
     */
    TeamMemberDTO updateMember(String teamBusinessId, String personBusinessId, MemberDTO access);

    /**
     * Revoke a member from the team.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The team business ID
     * @return The TeamMember DTO updated
     */
    TeamMemberDTO revokeMember(String teamBusinessId, String personBusinessId);

    /**
     * Promote a member from the team.
     * Stop the current access and grant the new one.
     * After the end of those new access, if the old access are still valid, the member will get those old access back.
     *
     * @param teamBusinessId   The team business ID
     * @param personBusinessId The team business ID
     * @param access           The new access for that person
     * @return The TeamMember DTO updated
     */
    TeamMemberDTO promoteMember(String teamBusinessId, String personBusinessId, MemberDTO access);
}
