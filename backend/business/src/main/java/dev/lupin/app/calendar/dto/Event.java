package dev.lupin.app.calendar.dto;

import dev.lupin.app.base.LupinDTO;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Event extends LupinDTO {

    private String title;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String description;



}
