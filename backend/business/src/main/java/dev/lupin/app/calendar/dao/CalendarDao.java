package dev.lupin.app.calendar.dao;

import dev.lupin.app.base.CrudDao;
import dev.lupin.app.calendar.dto.Calendar;
import dev.lupin.app.calendar.dto.Event;

import java.util.List;


public interface CalendarDao extends CrudDao<Calendar> {

    List<Event> findAllEventsForMonth(int year, int month);
}
