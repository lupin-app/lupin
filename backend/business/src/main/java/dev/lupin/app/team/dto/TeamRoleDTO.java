package dev.lupin.app.team.dto;

public enum TeamRoleDTO {
    OWNER,
    TEAM_MANAGER,
    MEMBER,
    REPORTER,
    GUEST;
}
